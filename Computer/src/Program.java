import java.util.ArrayList;
import java.util.List;

public class Program {

  private List<Instructions> instructionList;

  public Program() {
    instructionList = new ArrayList<>();
  }

  public void add(Instructions inst) {
    instructionList.add(inst);
  }

  public List<Instructions> getList() {
    return instructionList;
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    int nbr = 0;
    for (Instructions i : instructionList) {
        sb.append(nbr+ ": ");
        sb.append(i.toString() );
        nbr++;
    }
    return sb.toString();
    
  }
}
