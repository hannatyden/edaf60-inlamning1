public class LongWord implements Word {
  public Long value;

  public LongWord(String word) {
    this.value = Long.parseLong(word);
  }

 

  @Override
  public void add(Word w1, Word w2) {

    this.value = (long) ((LongWord) w1).value + ((LongWord) w2).value;
  }



  @Override
  public void mul(Word w1, Word w2) {

    this.value = (long) ((LongWord) w1).value * ((LongWord) w2).value;

  }

  @Override
  public void copy(Word word) {


    this.value = (long) ((LongWord) word).value;

  }

  @Override
  public boolean compare(Word word) {


    return value.equals((Long) ((LongWord) word).value);
  }

  @Override
  public String toString() {
    return "" + value;


  }

}

