
import java.util.List;

public class Computer {
  private Memory memory;
  private List<Instructions> programList;

  public Computer(Memory memory) {
    this.memory = memory;
  }

  public void load(Program program) {
    programList = program.getList();
  }


  public void run() {
    Counter counter = new Counter();
    while (counter.getCounter() != -1) {
      programList.get(counter.getCounter()).execute(counter, memory);
    }
  }
}
