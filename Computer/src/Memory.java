import java.util.ArrayList;
import java.util.List;

public class Memory {
  public List<Word> list;
  public int size;

  public Memory(int size, WordFactory wf) {
    this.list = new ArrayList<Word>();
    this.size = size;
  }

  public Word getValue(Address index) {

    return list.get(index.getPlaceInMemory());


  }

  public void setValue(Address index, Word word) {
    if (index.getPlaceInMemory() < size && index.getPlaceInMemory() >= 0) {
      list.add(index.getPlaceInMemory(), word);
    }



  }

 

}
