
public class Print implements Instructions {

  private Operand op;

  public Print(Operand op) {
    this.op = op;
  }

  @Override
  public void execute(Counter counter, Memory memory) {
    System.out.println(op.getWord(memory));
    counter.increment();

  }

  public String toString() {
    return "Print " + op.toString() + "\n";
  }


}


