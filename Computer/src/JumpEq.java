
public class JumpEq implements Instructions {

  private int n;
  private Operand op1;
  private Operand op2;

  public JumpEq(int n, Operand op1, Operand op2) {
    this.n = n;
    this.op1 = op1;
    this.op2 = op2;
  }

  @Override
  public void execute(Counter counter, Memory memory) {
    if (op1.getWord(memory).compare(op2.getWord(memory))) {
      counter.setCounter(n);
    } else {
      counter.increment();
    }


  }

  public String toString() {
    return "Jump to " + n + " if " + op1.toString() + " == " + op2.toString() + "\n";

  }



}
