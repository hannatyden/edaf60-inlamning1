
interface WordFactory {
  public Word word(String value);
}
