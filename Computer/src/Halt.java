
public class Halt implements Instructions {


  @Override
  public void execute(Counter counter, Memory memory) {
    counter.setCounter(-1);

  }

  public String toString() {
    return "Halt \n";
  }
}
