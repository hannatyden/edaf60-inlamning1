public class Copy implements Instructions {

  private Operand op;
  private Address addr;

  public Copy(Operand op, Address addr) {
    this.op = op;
    this.addr = addr;
  }

  @Override
  public void execute(Counter counter, Memory memory) {
    memory.setValue(addr, op.getWord(memory));
    counter.increment();

  }

  public String toString() {
    return "Copy " + op.toString() + " to " + addr.toString() + "\n";
  }



}
