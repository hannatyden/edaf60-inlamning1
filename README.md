# EDAF60-inlamning1

## Förberedelsefrågor
### 1. Det finns ett antal klasser och gränssnitt i programkoden ovan och det behövs ytterligare några för att kunna implementera programmet. Vilka klasser saknas och vilka är gränssnitt respektive klasser?
	Classes:
	ByteWordFactory, LongWordFactory, Program, Memory, Computer, Main
	Program.: Copy, JumpEq, Add, Jump, Print, Halt
	Interface: WordFactory, Instructions

### 2. Klassen Program har till synes metoden add för att lägga till en instruktion till programmet. Vilken standardklass skulle man kunna utvidga för att slippa implementera metoden själv? Är det lämpligt att göra så?
För att slippa  implementera en add-metod så kan standard klassen arraylist användas. Den är lämplig eftersom klassen har en add-metod med konstant tidskomplexitet och en sökningsmetod med  linjär tidskomplexitet. 

### 3. Klasserna skall fördelas på minst två paket. Vilka paket bör finnas och hur fördelas klasserna? (Under föreläsning 7 kommer vi att prata mer om paketindelning, men fundera gärna redan nu igenom vilka klasser som ‘hör ihop’ i detta projekt).
Package ‘computer’ innehåller: Program, Memory, Address,*WordFactory
	
### 4. Studera designmönstret Command. Var och hur bör det användas i uppgiften.
Detta kan användas för att deklarera alla kommandon: exempelvis ‘add’, ‘copy’, ‘ print’ osv i ett interface som klassen Program implementerar. Som Sum och Factorial sedan ärver från Program. Istället för att dessa metoder ska deklareras i både Sum och Factorial.

### 5. Studera designmönstret Template method. Mönstret skall användas för att undvika duplicerad kod i likartade klasser. Var kan detta bli aktuellt?
För att undvika duplicerad kod har vi valt att lägga till ett interface som Sum och Factorial, genom Program, ärver från.

### 6. Studera designmönstret Strategy och exemplen från föreläsningarna. Hur använder man mönstret för att hantera olika sorters operander på ett enhetligt sätt?
När man lägger ett kommando som ska användas av flera klasser högre upp i hierarkin så används dessa kommandon enhetligt i klasserna som ärver den.

### 7. När man exekverar Add-instruktionen i exemplen ovan skall man utföra en addition av två tal. I vilken klass skall den faktiska additionen utföras?
I klassen Program.

### 8. Rita ett sekvensdiagram på papper som visar alla inblandade objekt när Add-kommandot i Factorial exekveras. Ert diagram måste följa våra UML-anvisningar.
Se png-fil.

### 9. Vad bör hända om någon gör följande felaktiga anrop? run("factorial(5) med olika slags ord", new Factorial("5", lwf), bwf); (Observera att vi i detta exempel använder olika slags factories när vi skapar programmet och när vi skapar minnet.)
Ett kompileringsfel borde uppstå och ett felmeddelande bör dyka upp. Vi får ett kompileringsfel eftersom vi försöker göra en ByteWord till en LongWord. 
