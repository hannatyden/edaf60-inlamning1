package computer;

public abstract class BinOp implements Instruction {

	public Operand op1;
	public Operand op2;
	public Address resultAddress;

	public BinOp(Operand op1, Operand op2, Address resultAddress) {
		this.op1 = op1;
		this.op2 = op2;
		this.resultAddress = resultAddress;
	}

	public void execute(Counter counter, Memory memory) {
		op(op1.getWord(memory), op2.getWord(memory), resultAddress.getWord(memory));
		counter.increment();

	}

	abstract void op(Word op1, Word op2, Word result);

}
