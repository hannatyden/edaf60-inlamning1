package computer;

import java.util.List;

public class Computer {
	private Memory memory;
	private Program program;

	public Computer(Memory memory) {
		this.memory = memory;
	}

	public void load(Program program) {
		this.program = program;
	}

	public void run() {
		Counter counter = new Counter();
		while (counter.getCounter() != -1) {
			program.getInstruction(counter.getCounter()).execute(counter, memory);
		}
	}
}
