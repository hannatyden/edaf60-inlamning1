package computer;

import java.util.ArrayList;
import java.util.List;

public class Program implements Instruction {

	private List<Instruction> instructionList;

	public Program() {
		instructionList = new ArrayList<>();
	}

	public void add(Instruction inst) {
		instructionList.add(inst);
	}

	public Instruction getInstruction(int index) {
		return instructionList.get(index);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		int nbr = 0;
		for (Instruction i : instructionList) {
			sb.append(nbr + ": ");
			sb.append(i.toString());
			nbr++;
		}
		return sb.toString();

	}

	@Override
	public void execute(Counter counter, Memory memory) {
		// TODO Auto-generated method stub

	}
}
