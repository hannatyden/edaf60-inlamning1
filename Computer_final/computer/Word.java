package computer;
public interface Word extends Operand {

  public void add(Word w1, Word w2);

  public void mul(Word w1, Word w2);

  public void copy(Word word);

  public boolean compare(Word w1);

  public String toString();
  
  default public Word getWord(Memory memory) {
    return this;
  }

}
