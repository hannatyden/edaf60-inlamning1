package computer;

public class Add extends BinOp {

	public Add(Operand op1, Operand op2, Address resultAddress) {
		super(op1, op2, resultAddress);
	}

	@Override
	void op(Word op1, Word op2, Word resultAddress) {
		resultAddress.add(op1, op2);

	}

	public String toString() {
		return "Add " + op1.toString() + "  and " + op2.toString() + " into " + resultAddress.toString() + "\n";
	}

}
