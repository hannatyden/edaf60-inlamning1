package computer;
public class Address implements Operand {
  public int place;

  public Address(int a) {
    this.place = a;
  }



  public Word getWord(Memory memory) {

    return memory.list.get(place);

  }

  public int getPlaceInMemory() {
    return place;

  }


  public String toString() {
    return "[" + place + "]";
  }



}


