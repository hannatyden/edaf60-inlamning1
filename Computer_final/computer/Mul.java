package computer;

public class Mul extends BinOp {

	public Mul(Operand op1, Operand op2, Address resultAddress) {
		super(op1, op2, resultAddress);
	}

	@Override
	void op(Word op1, Word op2, Word resultAddress) {
		resultAddress.mul(op1, op2);

	}

	public String toString() {
		return "Multiply " + op1.toString() + "  and " + op2.toString() + " into " + resultAddress.toString() + "\n";
	}

}
