package computer;

interface Instruction {

	public void execute(Counter counter, Memory memory);

	public String toString();

	default void instructionsIndex() {
		this.toString();
	}

}
