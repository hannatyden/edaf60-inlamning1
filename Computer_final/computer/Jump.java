package computer;

public class Jump implements Instruction {

	private int n;

	public Jump(int n) {
		this.n = n;
	}

	@Override
	public void execute(Counter counter, Memory memory) {
		counter.setCounter(n);
	}

	public String toString() {
		return "Jump to " + n + "\n";

	}

}
