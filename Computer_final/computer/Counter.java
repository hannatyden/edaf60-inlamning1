package computer;

public class Counter {
	private int index;

	public Counter() {
		index = 0;
	}

	public void setCounter(int index) {
		this.index = index;
	}

	public int getCounter() {
		return index;
	}

	public void increment() {
		index++;

	}

}
