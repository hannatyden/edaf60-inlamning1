package wordFactory;

import computer.Word;

public class ByteWord implements Word {
	private Byte value;

	public ByteWord(String word) {
		this.value = Byte.parseByte(word);
	}

	@Override
	public void add(Word w1, Word w2) {

		this.value = (byte) (((ByteWord) w1).value + ((ByteWord) w2).value);
		;

	}

	@Override
	public void mul(Word w1, Word w2) {
		this.value = (byte) (((ByteWord) w1).value * ((ByteWord) w2).value);

	}

	@Override
	public void copy(Word word) {

		this.value = (byte) ((ByteWord) word).value;

	}

	@Override
	public boolean compare(Word word) {

		return value.equals((Byte) ((ByteWord) word).value);
	}

	@Override
	public String toString() {
		return "" + value;
	}

}
