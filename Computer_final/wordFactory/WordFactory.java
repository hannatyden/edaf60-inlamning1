package wordFactory;

import computer.Word;

public interface WordFactory {
	public Word word(String value);
}
